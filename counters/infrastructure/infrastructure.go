package infrastructure

import (
	"github.com/jinzhu/gorm"
	"github.com/prometheus/client_golang/prometheus"
)

// https://github.com/prometheus/client_golang/blob/master/prometheus/examples_test.go
type RuntimeCounters struct {
	NumActiveGoRoutines *prometheus.GaugeFunc
}

type DatabaseCounters struct {
	NumDatabaseConnections                   *prometheus.GaugeFunc
	IdleDatabaseConnections                  *prometheus.GaugeFunc
	DatabaseConnectionsInUse                 *prometheus.GaugeFunc
	BlockedWaitingTimeForDatabaseConnections *prometheus.GaugeFunc
	LatencyByOperation                       *prometheus.SummaryVec
}

type RequestCounters struct {
	RequestLatencySummaryCounters *prometheus.SummaryVec
	TotalRequestsCounters         *prometheus.CounterVec
}

type WorkerCounters struct {
	TotalCompletedTasksCounter         *prometheus.Counter
	TotalCompletedTasksByWorkerCounter *prometheus.CounterVec
}

type InfrastructureCounters struct {
	RuntimeCounters  *RuntimeCounters
	DatabaseCounters *DatabaseCounters
	RequestCounters  *RequestCounters
	WorkerCounters   *WorkerCounters
}

func New(serviceName string, db *gorm.DB) *InfrastructureCounters {
	return &InfrastructureCounters{
		RuntimeCounters:  InitRuntimeCounters(serviceName),
		DatabaseCounters: InitDatabaseCounters(serviceName, db),
		RequestCounters:  InitRequestCounters(serviceName),
		WorkerCounters:   InitWorkerCounters(serviceName),
	}
}
