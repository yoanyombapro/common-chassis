package infrastructure

import (
	"runtime"

	"github.com/prometheus/client_golang/prometheus"
)

// initialize run time counters
func InitRuntimeCounters(servicename string) *RuntimeCounters {
	numActiveGoRoutinesCounter := prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace: servicename,
			Subsystem: "runtime",
			Name:      "goroutines_count",
			Help:      "Number of goroutines that currently exist.",
		},
		func() float64 { return float64(runtime.NumGoroutine()) },
	)

	return &RuntimeCounters{
		NumActiveGoRoutines: &numActiveGoRoutinesCounter,
	}
}

func (rc *RuntimeCounters) RegisterMetrics(){
	_ =  RegisterGaugeMetric(*rc.NumActiveGoRoutines)
}
