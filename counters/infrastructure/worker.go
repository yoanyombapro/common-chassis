package infrastructure

import (
	"github.com/prometheus/client_golang/prometheus"
)

// initialize run time counters
func InitWorkerCounters(servicename string) *WorkerCounters {
	totalTasksCompleted := prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: servicename,
		Subsystem: "worker_pool",
		Name:      "completed_tasks_total",
		Help:      "Total number of tasks completed.",
	})

	// taskCounter.Inc()
	totalTasksCompletedPerWorker := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: servicename,
			Subsystem: "worker_pool",
			Name:      "completed_tasks_by_id",
			Help:      "Total number of tasks completed.",
		},
		[]string{"worker_id"},
	)
	// Each worker could also keep a reference to their own counter element
	// around. Pick the counter at initialization time of the worker.
	// myCounter := taskCounterVec.WithLabelValues("42") // From worker 42 initialization code.
	// myCounter.Inc()                                   // Somewhere in the code of that worker.
	return &WorkerCounters{
		TotalCompletedTasksCounter:        &totalTasksCompleted,
		TotalCompletedTasksByWorkerCounter: totalTasksCompletedPerWorker,
	}
}

func (rc *WorkerCounters) RegisterMetrics(){
	_ =  RegisterCounterMetric(*rc.TotalCompletedTasksCounter)
	_ = RegisterCounterVecMetric(*rc.TotalCompletedTasksByWorkerCounter)
}
